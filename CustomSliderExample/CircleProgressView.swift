//
//  CircleProgressView.swift
//  CustomSliderExample
//
//  Created by Javi Manzano on 06/09/2016.
//  Copyright © 2016 Javi Manzano. All rights reserved.
//

import UIKit

class CircleProgressView: UIControl {
    
    var progress: CGFloat = 0.0 {
        didSet {
            updateLayerFrames()
        }
    }

    let trackLayer = CircleLayer()
    
    // MARK: - Override functions
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        trackLayer.circleProgressView = self
        
        layer.addSublayer(trackLayer)
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
    }
    
    override var frame: CGRect {
        didSet {
            updateLayerFrames()
        }
    }
    
    func updateLayerFrames() {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        trackLayer.frame = frame
        trackLayer.setNeedsDisplay()
        
        CATransaction.commit()
    }

}
