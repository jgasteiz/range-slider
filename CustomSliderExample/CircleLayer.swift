//
//  CircleLayer.swift
//  CustomSliderExample
//
//  Created by Javi Manzano on 06/09/2016.
//  Copyright © 2016 Javi Manzano. All rights reserved.
//

import UIKit

class CircleLayer: CAShapeLayer {
    
    var progress: Int = 0
    
    weak var circleProgressView: CircleProgressView?
    
    override func drawInContext(ctx: CGContext) {
        if let circleProgressView = circleProgressView {
            path = UIBezierPath(
                roundedRect: CGRect(
                    x: 0,
                    y: 0,
                    width: circleProgressView.bounds.width,
                    height: circleProgressView.bounds.height),
                cornerRadius: circleProgressView.bounds.height / 2
                ).CGPath
            position = CGPoint(x: 0, y: 0)
            backgroundColor = nil
            anchorPoint = CGPoint.zero
            fillColor = nil
            
            strokeColor = UIColor.blueColor().CGColor
            strokeStart = 1 - circleProgressView.progress
            
            lineWidth = 4.0
        }
    }
}
