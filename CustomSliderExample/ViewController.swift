//
//  ViewController.swift
//  CustomSliderExample
//
//  Created by Javi Manzano on 06/09/2016.
//  Copyright © 2016 Javi Manzano. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let rangeSlider = RangeSlider(frame: CGRectZero)
    
    let circleProgressView = CircleProgressView(frame: CGRectZero)
    
    var progress: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(circleProgressView)
        
//        view.addSubview(rangeSlider)
//        
//        rangeSlider.addTarget(self, action: #selector(ViewController.rangeSliderValueChanged(_:)), forControlEvents: .ValueChanged)

        
        // Fake some progress
        _ = NSTimer.scheduledTimerWithTimeInterval(
            0.01,
            target: self,
            selector: #selector(update),
            userInfo: nil,
            repeats: true
        )
    }

    override func viewDidLayoutSubviews() {
        circleProgressView.backgroundColor = UIColor.greenColor()
        circleProgressView.frame = CGRect(x: 200.0,
                                          y: 200.0,
                                          width: 64.0,
                                          height: 64.0)
        

        let margin: CGFloat = 20.0
        let width = view.bounds.width - 2.0 * margin
        rangeSlider.frame = CGRect(x: margin,
                                   y: margin + topLayoutGuide.length,
                                   width: width,
                                   height: 31.0)
    }
    
    func update () {
        progress += CGFloat(arc4random_uniform(10)) / 1000.0
        circleProgressView.progress = progress
    }
    
    func rangeSliderValueChanged(rangeSlider: RangeSlider) {
        print("Range slider value changed: (\(rangeSlider.lowerValue) \(rangeSlider.upperValue))")
    }

}

